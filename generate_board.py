"""
Usage:
$ python generate_board.py <row_count> <column_count> <mine_probability(optional)>

"mine_probability" --> The probability that a block has a mine.
"""

__author__ = 'ankush'


import sys
from random import randint


def generate_board(m, n, prob):
	"""
	Input
	m --> row count
	n --> column count
	prob --> the probability that a block has a mine
	
	Output:
	board --> a (m x n) 2-D array, representing a block. Every entry in the array is an array of length 2.
	The first item represents adjacent mine counts. The second item represents whether the block has been visited(1) or not visited(0).

	Example output (for input m=3, n=3, prob=0.4):
	"board" with the following format:
	[2, 0]   [-1, 0]   [2, 0]
	[-1, 0]   [4, 0]   [-1, 0]
	[1, 0]   [3, 0]   [-1, 0]

	"""
	board = []
	P = prob*100
	for i in range(m):
		row = []
		for j in range(n):
			if (randint(1,100) <= P):
				row.append([-1,0])		#(-1,0) represents ("mine", "not visited")
			else:
				row.append([0,0])		#(0,0) represents ("not mine", "not visited")
		board.append(row)

	#Changing all 0s to adjacent mine counts
	for i in range(m):
		for j in range(n):
			if board[i][j][0] == -1:
				continue
			count = 0
			if i<(m-1):
				if j<(n-1):
					if board[i+1][j+1][0] == -1:
						count += 1
				if j>0:
					if board[i+1][j-1][0] == -1:
						count += 1
				if board[i+1][j][0] == -1:
					count += 1
			if i>0:
				if j<n-1:
					if board[i-1][j+1][0] == -1:
						count += 1
				if j>0:
					if board[i-1][j-1][0] == -1:
						count += 1
				if board[i-1][j][0] == -1:
					count += 1

			if j<n-1:
				if board[i][j+1][0] == -1:
					count += 1
			if j>0:
				if board[i][j-1][0] == -1:
					count += 1
			board[i][j][0] = count

	return board



def print_complete_board(board, m, n):
	"""
	This function prints the entire board with all the values
	"""
	for i in range(m):
		for j in range(n):
			print board[i][j], ' ',
		print


def print_board(board, m, n):
	"""
	This function prints the board visible to the player
	"x" represents blocks not visited by the player
	"""
	for i in range(m):
		for j in range(n):
			if board[i][j][1] == 1:    #explored
				if board[i][j][0] == -1:
					print 'M ',
				else:	
					print board[i][j][0], ' ',
			elif board[i][j][1] == 2:    #marked as mine
				print 'x', ' ',
			else:
				print '-', ' ',    #unexplored
		print


def count_mines(board, m, n):
	"""
	Output: the count of the number of mines in the board
	"""
	count = 0
	for i in range(m):
		for j in range(n):
			if board[i][j][0] == -1:
				count += 1
	return count



if __name__ == '__main__':
	m = int(sys.argv[1])
	n = int(sys.argv[2])
	if len(sys.argv) > 3:
		prob = float(sys.argv[3])
	else:
		prob = 0.25
	board = generate_board(m, n, prob)
	print_complete_board(board, m, n)
