"""
Usage:
$ python play_game.py <row_count> <column_count> <mine_probability(optional)>

The board state is printed and the user must enter space separated row and column values as their move.
The game stops when a mine is encountered, or when all blocks have been explored.

Example run:
$ python play_game.py 3 3 0.2
x   x   x
x   x   x
x   x   x

1 1
0   x   x
x   x   x
x   x   x

1 3
0   x   -1
x   x   x
x   x   x

You Lose!
"""

__author__ = 'ankush'


from generate_board import generate_board, print_board, count_mines
import sys


def play_game(board, m, n):
	blocks_count = m*n
	mines_count = count_mines(board, m, n)
	non_mines_count = blocks_count - mines_count
	explored_count = 0
	print_board(board, m, n)
	print
	while True:
		move_i, move_j = [(int(index)-1) for index in raw_input().split()]
		if move_i>=m or move_j>=n or move_i<0 or move_j<0:
			print "Invalid input!"
			print
			continue
		board[move_i][move_j][1] = 1	#marking visited
		explored_count += 1
		print_board(board, m, n)
		print
		if board[move_i][move_j][0] == -1:		#Game stops when mine encountered
			print "You Lose!"
			break
		if explored_count == non_mines_count:
			print "You Win!"
			break


if __name__ == '__main__':
	m = int(sys.argv[1])
	n = int(sys.argv[2])
	if len(sys.argv) > 3:
		prob = float(sys.argv[3])
	else:
		prob = 0.25
	board = generate_board(m, n, prob)
	play_game(board, m, n)
