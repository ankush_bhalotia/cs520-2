* "generate_board.py" created. Usage explained in the file.

* Board representation has been explained in the docstring for the function "generate_board()" in "generate_board.py".

* "play_game.py" implements the game using the board generated.


How to Run (Python 2):
$ python solver.py [ [m -board height] [n -board width] [p -optional probability, default 0.1]]


Examples:

$ python solver.py 10 10

$ python solver.py 50 50 0.2
