"""
AI solver for minesweeper

Usage:
$ python solver.py <row_count> <column_count> <mine_probability(optional)>

"mine_probability" --> The probability that a block has a mine.
"""


__author__ = 'ankush, shyam, devashree, monika'


import sys
import random
from random import randint
from generate_board import generate_board, print_board, count_mines, print_complete_board
import numpy as np
from itertools import combinations
import copy


def make_random_move(board, m, n, moves):
	"""
		It is making an intelligent, informed random move based on probability values instead of a truly random move
		Calculates the probability of exploring a cell close to constrained cells (cur_prob); compares it to a threshold value of 0.5
		Decides whether to make a truly random move or not; returns False if a mine is encountered, returns True otherwise
	"""
	best_prob = 100.0
	pool = []
	prob = []
	for i in range(m):
		row_prob = []
		for j in range(n):
			if board[i][j][1]>0:
				row_prob.append(100.0);
				continue
			cur_prob = 0.0
			for (row,col) in get_neighbor_blocks(board, m, n, (i,j), explored=True):
				cur_prob += float(board[row][col][0])/float(len(get_neighbor_blocks(board, m, n, (row,col), unexplored=True)))
			if cur_prob == 0.0:
				cur_prob = 0.5 #Threshold
			if best_prob == cur_prob:
				pool.append((i,j))
			if best_prob > cur_prob:
				while pool:
					pool.pop()
				pool.append((i,j))
				best_prob = cur_prob	
			row_prob.append(cur_prob)
		prob.append(row_prob)
	move = random.choice(pool)
	while board[move[0]][move[1]][1]==1:   #Discarding move if already visited
		move = (randint(0,m-1), randint(0,n-1))

	return make_move(board, m, n, move, moves)



def make_truly_random_move(board, m, n, moves):
	"""
		Makes a random move on the board; returns False if a mine is encountered, returns True otherwise
	"""
	move = (randint(0,m-1), randint(0,n-1))
	while board[move[0]][move[1]][1]==1:   #Discarding move if already visited
		move = (randint(0,m-1), randint(0,n-1))
	board[move[0]][move[1]][1] = 1    #marking visited
	moves.append(move)
	if board[move[0]][move[1]][0] == -1:
		return False
	return True



def make_move(board, m, n, move, moves):
	"""
		Makes "move" (as provided) on the board; returns False if a mine is encountered, returns True otherwise
	"""	
	board[move[0]][move[1]][1] = 1    #marking visited
	moves.append(move)
	print_board(board, m, n)	
	if board[move[0]][move[1]][0] == -1:
		return False
	print "--"*10
	return True



def mark_mine(board, position):
	board[position[0]][position[1]][1] = 2  #2 represents "marked as mine"



def is_marked_mine(board, position):
	if board[position[0]][position[1]][1] == 2:  #2 represents "marked as mine"
		return True
	return False


def is_unexplored(board, position):
	if board[position[0]][position[1]][1] == 0:  #0 represents "unexplored"
		return True
	else:
		return False



def get_neighbor_blocks(board, m, n, move, explored=False, unexplored=False, marked_mine=False):
	"""
		Returns neighbor blocks
	"""
	type_vals = []
	if explored:
		type_vals.append(1)
	if unexplored:
		type_vals.append(0)
	if marked_mine:
		type_vals.append(2)

	if not type_vals:
		return []

	neighbor_blocks = []

	if move[0] < m-1:
		if move[1] < n-1:
			if board[move[0] + 1][move[1] + 1][1] in type_vals:
				neighbor_blocks.append((move[0] + 1, move[1] + 1))
		if move[1] > 0:
			if board[move[0] + 1][move[1] - 1][1] in type_vals:
				neighbor_blocks.append((move[0] + 1, move[1] - 1))
		if board[move[0] + 1][move[1]][1] in type_vals:
			neighbor_blocks.append((move[0] + 1, move[1]))

	if move[0] > 0:
		if move[1] < n-1:
			if board[move[0] - 1][move[1] + 1][1] in type_vals:
				neighbor_blocks.append((move[0] - 1, move[1] + 1))
		if move[1] > 0:
			if board[move[0] - 1][move[1] - 1][1] in type_vals:
				neighbor_blocks.append((move[0] - 1, move[1] - 1))
		if board[move[0] - 1][move[1]][1] in type_vals:
			neighbor_blocks.append((move[0] - 1, move[1]))

	if move[1] < n-1:
		if board[move[0]][move[1] + 1][1] in type_vals:
			neighbor_blocks.append((move[0], move[1] + 1))

	if move[1] > 0:
		if board[move[0]][move[1] - 1][1] in type_vals:
			neighbor_blocks.append((move[0], move[1] - 1))

	return neighbor_blocks




def explore_all_neighbors(board, m, n, move, moves):
	"""
		Explores all available neighbor blocks 
	"""
	neighbor_blocks = get_neighbor_blocks(board, m, n, move, unexplored=True)
	for block in neighbor_blocks:
		make_move(board, m, n, block, moves)




def explore_random_neighbor(board, m, n, move, moves):
	"""
		Explores a random unexplored neighbor
	"""
	neighbor_blocks = get_neighbor_blocks(board, m, n, move, unexplored=True)
	random_index = randint(0,len(neighbor_blocks))
	return make_move(board, m, n, neighbor_blocks[random_index], moves)




def get_linear_equations_matrix(board, m, n, moves):
	"""
	Given a set of moves, returns a set of linear equations (in matrix form) along with the corresponding variables
	(variables are unexplored blocks on the board)

	The linear equations represent board constraints for the given set of variables.
	The last column in the matrix represents the value of the respective linear equation.

	Example:
	"variables" (list of tuples):
	[(0, 1), (3, 2), (0, 2), (3, 0), (2, 1), (1, 1), (1, 3), (2, 3), (1, 0), (0, 3)]
	
	"matrix":
	[[ 0  0  0  1  1  1  0  0  1  0  0]
	 [ 1  0  1  0  1  1  1  1  0  1  0]
	 [ 0  1  0  0  1  1  1  1  0  0  1]
	 [ 0  1  0  1  1  0  0  0  0  0  1]
	 [ 0  1  0  0  0  0  0  1  0  0  1]]
	"""
	equations = []
	for move in moves:
		neighbor_blocks = get_neighbor_blocks(board, m, n, move, unexplored=True, marked_mine=True)
		move_val = board[move[0]][move[1]][0]
		equations.append([neighbor_blocks, move_val])
	variables = set()
	for equation in equations:
		for var in equation[0]:
			variables.add(var)
	variables = list(variables)
	matrix = np.zeros(shape=(len(equations), len(variables)+1), dtype=np.int)
	for eq_ind,equation in enumerate(equations):
		for var_ind,var in enumerate(variables):
			if var in equation[0]:
				matrix[eq_ind][var_ind] = 1
		matrix[eq_ind][-1] = equation[1]

	marked_as_mines = set()
	for var_ind, var in enumerate(variables):
		if is_marked_mine(board, var):
			marked_as_mines.add(var_ind)

	#Eliminating variables from the matrix that have been marked as mines
	for var_ind in marked_as_mines:
		matrix[:,-1] = matrix[:,-1] - matrix[:,var_ind]
	matrix = np.delete(matrix, list(marked_as_mines), axis=1)
	new_variables = []
	for ind in range(len(variables)):
		if ind in marked_as_mines:
			continue
		new_variables.append(variables[ind])
	variables = new_variables

	return variables, matrix



def get_disjoint_matrices(variables, matrix):
	#( O(len(variables)^2) ) Can this be optimized?
	"""
	Returns a list of disjoint matrices and corresponding variables
	Example output:
	[
	  {
	    "variables": [(0, 7), (1, 6), (1, 5), (1, 7), (0, 5)],
	    "matrix": array([[ 1,  1,  1,  1,  1,  4]])
	  },
	  ...
	  {
		"variables": ...
		"matrix": ...
	  }
	]
	"""

	var_eq_mapping = {x:set() for x in range(len(variables))}  #maps variable indices to indices of equations that contain them
	for i in range(len(variables)):
		for j,row in enumerate(matrix):
			if row[i]==1:
				var_eq_mapping[i].add(j)

	#If equations have overlapping variables, we merge them
	for i in range(len(variables)):
		for j in range(len(variables)):
			if i==j:
				continue
			if var_eq_mapping[i].intersection(var_eq_mapping[j]):
				var_eq_mapping[i] = var_eq_mapping[i].union(var_eq_mapping[j])
				var_eq_mapping[j] = var_eq_mapping[i].union(var_eq_mapping[j])

	eq_var_mapping = {}
	for var,eq in var_eq_mapping.iteritems():	
		eq = frozenset(eq)
		if eq_var_mapping.get(eq):
			eq_var_mapping[eq].append(var)
		else:
			eq_var_mapping[eq] = [var]

	disjoint_matrices = []
	for eq, var in eq_var_mapping.iteritems():
		output_mat = np.zeros(shape=(len(eq), len(var)+1), dtype=np.int)
		for row_no, row_ind in enumerate(eq):
			for var_no, var_ind in enumerate(var):
				output_mat[row_no][var_no] = matrix[row_ind][var_ind]
			output_mat[row_no][-1] = matrix[row_ind][-1]
		output_var = []
		for var_ind in var:
			output_var.append(variables[var_ind])
		disjoint_matrices.append({"variables": output_var, "matrix": output_mat})

	return disjoint_matrices




def solve_trivial_cases(board, m, n, variables, matrix, moves):
	flag = False
	for row in matrix:
		if row[-1] == 0:
			for ind in range(len(row)-1):
				col = row[ind]
				if col==1 and is_unexplored(board, variables[ind])==True:
					flag = True
					if make_move(board, m, n, variables[ind], moves) == False:
						print "Trivial failed"

		#If the number of variables is equal to the number of mines, all of them can be considered mines
		if np.sum(row[:-1])==row[-1] and row[-1]!=0:
			for ind in range(len(row)-1):
				col = row[ind]
				if col==1:
					flag = True
					mark_mine(board, variables[ind])

	return flag



def is_matrix_consistent(matrix):
	if matrix.shape[1] == 1:    #If only one column is left (the value column), all the values must be zero
		for row in matrix:
			if row[0] != 0:
				return False
		return True
	else:       #The number of variables in an equation must be greater than or equal to the number of mines
		for row in matrix:
			if np.sum(row[:-1]) >= row[-1]:
				return False
		return True



def remove_duplicate_rows(matrix):
	to_delete = []
	for ind1 in range(matrix.shape[0]-1):
		row1 = matrix[ind1]
		for ind2 in range(ind1+1, matrix.shape[0]):
			row2 = matrix[ind2]
			flag = True
			for item_ind, item in enumerate(row1):
				if row2[item_ind] != item:
					flag = False
					break
			if flag==True:
				to_delete.append(ind2)
	matrix = np.delete(matrix, to_delete, axis=0)
	return matrix




def solve_linear_equations_matrix(board, m, n, variables, matrix):

	#2D columns, first column contains solution values
	#second column contains 0 if the solution value is variable
	solution = np.zeros(shape=[len(variables), 1], dtype=np.int)

	count_var = len(variables)

	stack = []
	stack.append({
			"matrix": matrix,
			"variables": variables,
			"mine_positions": [],
			"not_mine_positions": []
		})
	
	while stack:
		state = stack.pop()

		#removing redundant rows from state matrix
		to_delete = []
		for row_ind, row in enumerate(state["matrix"]):
			if np.sum(row)==0:
				to_delete.append(row_ind)
		state["matrix"] = np.delete(state["matrix"], to_delete, axis=1)
		
		#removing duplicate rows
		state["matrix"] = remove_duplicate_rows(state["matrix"])


		if state["matrix"].size==0:
			return state["mine_positions"], state["not_mine_positions"]


		#if the rank of the matrix is less than the number of variables
		if np.linalg.matrix_rank(state["matrix"]) < (state["matrix"].shape[1] - 1):
			break

		var_inds = [i for i,x in enumerate(state["matrix"][0][:-1]) if x==1]
		combns = combinations([v for v in var_inds], state["matrix"][0][-1])
		for mine_inds in combns:
			mine_inds = set(mine_inds)
			case_matrix = copy.deepcopy(state["matrix"])
			for ind in mine_inds:
				case_matrix[:,-1] = case_matrix[:,-1] - case_matrix[:,ind]
			not_mine_inds = set(var_inds) - mine_inds				
			case_matrix = np.delete(case_matrix, list(mine_inds) + list(not_mine_inds), axis=1)  #eliminating variables
			case_matrix = case_matrix[1:]  #eliminating the first (current) row
			case_matrix = remove_duplicate_rows(case_matrix)
			case_variables = []
			for ind in range(len(state["variables"])):
				if ind in mine_inds or ind in not_mine_inds:
					continue
				case_variables.append(state["variables"][ind])
			if not is_matrix_consistent(case_matrix):
				continue
			stack.append({
				"matrix": case_matrix,
				"variables": case_variables,
				"mine_positions": state["mine_positions"] + [state["variables"][ind] for ind in mine_inds],
				"not_mine_positions": state["not_mine_positions"] + [state["variables"][ind] for ind in not_mine_inds]
				})

	return [], []


def visited_count(board,m,n):
	count = 0
	for row in board:
		for col in row:
			if col[1]>0:
				count += 1
	return count


def solver(board, m, n, moves, make_random=False):
	if make_random==True:
		if make_random_move(board, m, n, moves)==False:
			return False

	variables, matrix = get_linear_equations_matrix(board, m, n, moves)
	while solve_trivial_cases(board, m, n, variables, matrix, moves):
		variables, matrix = get_linear_equations_matrix(board, m, n, moves)

	disjoint_matrices = get_disjoint_matrices(variables, matrix)
	if len(disjoint_matrices)==0:

		if(visited_count(board,m,n)==m*n):
			print_board(board, m, n)
			return True
		else:
			return solver(board, m, n, moves, make_random=True)

	for var_mat in disjoint_matrices:
		mine_positions, not_mine_positions = solve_linear_equations_matrix(board, m, n, var_mat["variables"], var_mat["matrix"])
		if len(mine_positions)==0 and len(not_mine_positions)==0:			
			return solver(board, m, n, moves, make_random=True)
		else:
			for not_mine_pos in not_mine_positions:
				make_move(board, m, n, not_mine_pos, moves)
			for mine_pos in mine_positions:
				mark_mine(board, mine_pos)
			return solver(board, m, n, moves)



if __name__ == '__main__':
	m = int(sys.argv[1])
	n = int(sys.argv[2])
	if len(sys.argv) > 3:
		prob = float(sys.argv[3])
	else:
		prob = 0.10
	count_tries = 0
	board = generate_board(m, n, prob)
	# while True:
	board_new = copy.deepcopy(board)
	count_tries+=1
	
	if solver(board_new, m, n, [], make_random=True)==True:
		print "Solved"
	else:
		print "Not solved"
